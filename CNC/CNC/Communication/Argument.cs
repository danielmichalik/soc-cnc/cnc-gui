﻿using System;
using System.Text.RegularExpressions;

namespace CNC.Communication
{
    public class Argument
    {
        private ArgumentType _argumentType;

        public int IntNumber { get; private set; } = 0;
        public double DoubleNumber { get; private set; } = 0;
        public string StringPart { get; private set; } = string.Empty;

        public Argument(int value)
        {
            IntNumber = value;
            _argumentType = ArgumentType.IntNumber;
        }

        public Argument(double value)
        {
            DoubleNumber = value;
            _argumentType = ArgumentType.DoubleNumber;
        }

        public Argument(string prefix, int value)
        {
            IntNumber = value;
            StringPart = prefix;
            _argumentType = ArgumentType.NamedIntNumber;
        }

        public Argument(string prefix, double value)
        {
            DoubleNumber = value;
            StringPart = prefix;
            _argumentType = ArgumentType.NamedDoubleNumber;
        }

        public Argument(string content)
        {
            StringPart = content;
            _argumentType = ArgumentType.StringOnly;
        }

        public override string ToString()
        {
            switch (_argumentType)
            {
                case ArgumentType.IntNumber:
                    return $"{IntNumber}";
                case ArgumentType.DoubleNumber:
                    return $"{DoubleNumber:N3}";
                case ArgumentType.NamedIntNumber:
                    return $"{StringPart}{IntNumber}";
                case ArgumentType.NamedDoubleNumber:
                    return $"{StringPart}{DoubleNumber:N3}";
                case ArgumentType.StringOnly:
                    return StringPart;
                default:
                    throw new Exception("Unsupperoted");
            }
        }

        public static Argument Parse(string input)
        {
            Regex namedDoubleRegex = new Regex(@"[a-zA-Z]+\d+[\.,]\d+");
            Regex namedIntRegex = new Regex(@"[a-zA-Z]+\d+");
            Regex doubleRegex = new Regex(@"\d+[\.,]\d+");
            Regex intRegex = new Regex(@"\d+");

            if (namedDoubleRegex.IsMatch(input))
            {
                FixDecimalSeparator(ref input);
                Regex namedDoubleGroupRegex = new Regex(@"([a-zA-Z]+)(\d+[\.,]\d+)");
                var match = namedDoubleGroupRegex.Match(input);
                string stringPart = match.Groups[1].Value;
                string doublePart = match.Groups[2].Value;
                double doubleNumber = double.Parse(doublePart);
                return new Argument(stringPart, doubleNumber);
            }
            else if (namedIntRegex.IsMatch(input))
            {
                Regex namedIntGroupRegex = new Regex(@"([a-zA-Z]+)(\d+)");
                var match = namedIntGroupRegex.Match(input);
                string stringPart = match.Groups[1].Value;
                string intPart = match.Groups[2].Value;
                int intNumber = int.Parse(intPart);
                return new Argument(stringPart, intNumber);
            }
            else if (doubleRegex.IsMatch(input))
            {
                FixDecimalSeparator(ref input);
                double number = double.Parse(input);
                return new Argument(number);
            }
            else if (intRegex.IsMatch(input))
            {
                int number = int.Parse(input);
                return new Argument(number);
            }

            return new Argument(input);
        }

        private static void FixDecimalSeparator(ref string data)
        {
            var decimalSeparator = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
            char decimalSeparatorToReplace = (decimalSeparator == ",") ? '.' : ',';
            data = data.Replace(decimalSeparatorToReplace, decimalSeparator[0]);
        }
    }
}
