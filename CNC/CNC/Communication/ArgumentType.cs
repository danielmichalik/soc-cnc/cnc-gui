﻿namespace CNC.Communication
{
    public enum ArgumentType
    {
        IntNumber,
        DoubleNumber,
        NamedIntNumber,
        NamedDoubleNumber,
        StringOnly
    }
}
