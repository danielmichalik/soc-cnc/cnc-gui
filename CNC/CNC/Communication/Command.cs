﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CNC.Communication
{
    public class Command
    {
        private const char _startChar = '\u0002';   // STX
        private const char _endChar = '\u0003';     // ETX
        private const bool _endWithNewLine = true;  // end with \r\n
        public CommandType Type { get; set; }
        public List<Argument> Arguments { get; set; }

        public Command(CommandType type, List<Argument> arguments = null)
        {
            Type = type;
            Arguments = arguments ?? new List<Argument>();
        }

        public override string ToString()
        {
            string endOfCommand = _endWithNewLine ? $"{_endChar}\r\n" : $"{_endChar}";
            List<string> contentList = new List<string>
            {
                Enum.GetName(typeof(CommandType), Type)
            };
            contentList.AddRange(Arguments.ConvertAll(s => s.ToString()));
            string contentString = string.Join(";", contentList.ToArray());
            return $"{_startChar}{contentString}{endOfCommand}";
        }

        public static Command Parse(string commandString)
        {
            var regex = new Regex("\u0002(.+)\u0003");
            var match = regex.Match(commandString);

            if (match.Success)
            {
                var innerCommand = match.Groups[1].Value;
                var partsOfCommand = new List<string>(innerCommand.Split(';'));
                CommandType typeOfCommand = (CommandType)Enum.Parse(typeof(CommandType), partsOfCommand[0]);
                partsOfCommand.RemoveAt(0);
                List<Argument> listOfArguments = new List<Argument>();
                listOfArguments.AddRange(partsOfCommand.ConvertAll(s => Argument.Parse(s)));
                return new Command(typeOfCommand, listOfArguments);
            }

            return null;
        }
    }
}
