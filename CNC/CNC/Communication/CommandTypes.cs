﻿namespace CNC.Communication
{
    public enum CommandType
    {
        UNKNOWN,
        ECHO,
        G_CODE,
        GET_POS,
        START,
        STOP,
        RESET,
        STEP,
        OK,
        ERR,
        SET_SPD_MULT,
        ZERO,
        GET_ACT_ID,
        RUN_SPINDLE,
        STOP_SPINDLE,
        DISP_TEST,
        GET_BUFF
    }
}
