﻿using CNC.Communication;
using CNC.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CNC.ViewModel
{
    public class ToolControlViewModel : INotifyPropertyChanged
    {
        public ToolControlViewModel(CommunicationCore communicationCore)
        {
            _communicationCore = communicationCore;
        }

        ~ToolControlViewModel()
        {
            cts.Cancel();
        }

        private CancellationTokenSource cts = new CancellationTokenSource();
        private CommunicationCore _communicationCore;

        private int _spindleRpm = 1500;
        public int SpindleRpm
        {
            get
            {
                return _spindleRpm;
            }
            set
            {
                _spindleRpm = value;
                OnPropertyChanged(nameof(SpindleRpm));
            }
        }

        private int _dispTime = 45;
        public int DispTime
        {
            get
            {
                return _dispTime;
            }
            set
            {
                _dispTime = value;
                OnPropertyChanged(nameof(DispTime));
            }
        }

        public ObservableCollection<string> SpindleDirections
        {
            get
            {
                var spindleDirections = new List<string>
                {
                    "CW",
                    "CCW",
                };
                return new ObservableCollection<string>(spindleDirections);
            }
        }

        public string SpindleDirectionSelected
        {
            get;
            set;
        }

        private ICommand _spindleRunCommand;
        public ICommand SpindleRunCommand
        {
            get
            {
                return _spindleRunCommand ?? (_spindleRunCommand = new CommandHandler(() => SpindleRunCommandAction(), true));
            }
        }

        private ICommand _dispTestCommand;
        public ICommand DispTestCommand
        {
            get
            {
                return _dispTestCommand ?? (_dispTestCommand = new CommandHandler(() => DispTestCommandAction(), true));
            }
        }

        private bool _isRunning = false;
        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }

            set
            {
                _isRunning = value;
                OnPropertyChanged(nameof(IsRunning));
            }
        }

        private void SpindleRunCommandAction()
        {
            if (!_communicationCore.IsConnected)
            {
                MessageBox.Show("Systém není připojen!", "Chyba");
                return;
            }

            if (IsRunning)
            {
                StopSpindle();
            }
            else
            {
                StartSpindle();
            }
        }

        private void DispTestCommandAction()
        {
            if (!_communicationCore.IsConnected)
            {
                MessageBox.Show("Systém není připojen!", "Chyba");
                return;
            }

            Task.Run(async () => {
                var listOfArgs = new List<Argument>() { new Argument(DispTime) };
                var command = new Command(CommandType.DISP_TEST, listOfArgs);
                while (_communicationCore.IsBlocked) ;
                var response = await _communicationCore.TransactionComPort(command);
                if (response.Type == CommandType.ERR)
                {
                    MessageBox.Show($"Při zpracování příkazu DISP_TEST došlo k chybě!", "Chyba");
                }
            });
        }

        private void StartSpindle()
        {
            Task.Run(async () => {
                var listOfArgs = new List<Argument>() { new Argument(SpindleRpm) };
                var command = new Command(CommandType.RUN_SPINDLE, listOfArgs);
                while (_communicationCore.IsBlocked) ;
                var response = await _communicationCore.TransactionComPort(command);
                if (response.Type == CommandType.ERR)
                {
                    MessageBox.Show($"Při zpracování příkazu RUN_SPINDLE došlo k chybě!", "Chyba");
                }
                else
                {
                    IsRunning = true;
                }
            });
        }

        private void StopSpindle()
        {
            Task.Run(async () => {
                while (_communicationCore.IsBlocked) ;
                var response = await _communicationCore.TransactionComPort(new Command(CommandType.STOP_SPINDLE));
                if (response.Type == CommandType.ERR)
                {
                    MessageBox.Show($"Při zpracování příkazu STOP_SPINDLE došlo k chybě!", "Chyba");
                }
                else
                {
                    IsRunning = false;
                }
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
