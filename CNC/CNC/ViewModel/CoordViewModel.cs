﻿using CNC.Model;
using CNC.Communication;
using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace CNC.ViewModel
{
    public class CoordViewModel : INotifyPropertyChanged
    {
        public CoordViewModel(CommunicationCore communicationCore)
        {
            _communicationCore = communicationCore;
        }

        ~CoordViewModel()
        {
            cts.Cancel();
        }

        private CancellationTokenSource cts = new CancellationTokenSource();
        private CommunicationCore _communicationCore;
        private const string _coordStringFormat = "{0:+0000.0000;-0000.0000;+0000.0000;}";

        public event PropertyChangedEventHandler PropertyChanged;

        private double _coordX = 0;
        public string CoordX
        {
            get
            {
                return string.Format(_coordStringFormat, _coordX);
            }
        }

        private double _coordY = 0;
        public string CoordY
        {
            get
            {
                return string.Format(_coordStringFormat, _coordY);
            }
        }

        private double _coordZ = 0;
        public string CoordZ
        {
            get
            {
                return string.Format(_coordStringFormat, _coordZ);
            }
        }

        public void StartCoordUpdateTask()
        {
            Task<Task> task = Task.Factory.StartNew(async () =>
            {
                while (!cts.IsCancellationRequested)
                {
                    await Task.Delay(1000);

                    if (!_communicationCore.IsConnected)
                    {
                        continue;
                    }

                    var actualCoords = await GetCoordsAsync();
                    _coordX = actualCoords.Item1;
                    _coordY = actualCoords.Item2;
                    _coordZ = actualCoords.Item3;
                    OnPropertyChanged(nameof(CoordX));
                    OnPropertyChanged(nameof(CoordY));
                    OnPropertyChanged(nameof(CoordZ));
                }
            }, TaskCreationOptions.LongRunning);

            Task actualTask = task.Unwrap();
        }

        private ICommand _setZeroXCommand;
        public ICommand SetZeroXCommand
        {
            get
            {
                return _setZeroXCommand ?? (_setZeroXCommand = new CommandHandler(() => SetAxisZeroCommandAction("X"), true));
            }
        }

        private ICommand _setZeroYCommand;
        public ICommand SetZeroYCommand
        {
            get
            {
                return _setZeroYCommand ?? (_setZeroYCommand = new CommandHandler(() => SetAxisZeroCommandAction("Y"), true));
            }
        }

        private ICommand _setZeroZCommand;
        public ICommand SetZeroZCommand
        {
            get
            {
                return _setZeroZCommand ?? (_setZeroZCommand = new CommandHandler(() => SetAxisZeroCommandAction("Z"), true));
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private async Task<Tuple<double, double, double>> GetCoordsAsync()
        {
            var response = await _communicationCore.TransactionComPort(new Command(CommandType.GET_POS));
            if (response.Type == CommandType.GET_POS)
                return new Tuple<double, double, double>(
                    response.Arguments[0].DoubleNumber,
                    response.Arguments[1].DoubleNumber,
                    response.Arguments[2].DoubleNumber);
            return new Tuple<double, double, double>(0, 0, 0);
        }

        private void SetAxisZeroCommandAction(string axisId)
        {
            Regex correctAxisIdsRegex = new Regex(@"^[X,Y,Z]$");
            if (!correctAxisIdsRegex.IsMatch(axisId))
            {
                MessageBox.Show($"Osa {axisId} neexistuje!", "Chyba");
                return;
            }

            if (!_communicationCore.IsConnected)
            {
                MessageBox.Show("Systém není připojen!", "Chyba");
                return;
            }

            Task.Run(async () =>
            {
                while (_communicationCore.IsBlocked) ;
                var listOfArgs = new List<Argument>() { new Argument(axisId) };
                var command = new Command(CommandType.ZERO, listOfArgs);
                var response = await _communicationCore.TransactionComPort(command);
                if (response.Type == CommandType.ERR)
                {
                    MessageBox.Show($"Při zpracování příkazu ZERO došlo k chybě!", "Chyba");
                }
            });
        }
    }
}
