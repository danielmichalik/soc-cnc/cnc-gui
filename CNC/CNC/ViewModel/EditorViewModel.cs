﻿using CNC.Model;
using System.ComponentModel;
using System.IO;
using System.Windows.Documents;
using System.Windows.Input;

namespace CNC.ViewModel
{
    class EditorViewModel : INotifyPropertyChanged
    {
        public EditorViewModel(FileHelper fileHelper, CommunicationCore communicationCore, GCodeKeep gCodeKeep)
        {
            _fileHelper = fileHelper;
            _communicationCore = communicationCore;
            _gCodeKeep = gCodeKeep;
        }

        private FileHelper _fileHelper;
        private CommunicationCore _communicationCore;
        private GCodeKeep _gCodeKeep;

        private FlowDocument _editorContent;
        public FlowDocument EditorContent
        {
            get
            {
                return _editorContent;
            }
            set
            {
                _editorContent = value;
                OnPropertyChanged("EditorContent");
            }
        }

        private ICommand _openCommand;
        public ICommand OpenCommand
        {
            get
            {
                return _openCommand ?? (_openCommand = new CommandHandler(() => OpenCommandAction(), true));
            }
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                return _saveCommand ?? (_saveCommand = new CommandHandler(() => SaveCommandAction(), true));
            }
        }

        private ICommand _closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                return _closeCommand ?? (_closeCommand = new CommandHandler(() => CloseCommandAction(), true));
            }
        }

        public void OpenCommandAction()
        {
            _fileHelper.ShowOpenFileDialog();
            if (_fileHelper.Path == "")
            {
                // vsechno spatne hlasku a 
                return;
            }

            _gCodeKeep.LoadGCode();
            
            FlowDocument flowDocument = new FlowDocument();
            using (var streamReader = new StreamReader(_fileHelper.Path))
            {
                string line = streamReader.ReadLine();
                while(line != null)
                {
                    var paragraph = new Paragraph(new Run(line));
                    flowDocument.Blocks.Add(paragraph);
                    line = streamReader.ReadLine();
                }
            }
            EditorContent = flowDocument;
        }

        public void SaveCommandAction()
        {
            var textRange = new TextRange(EditorContent.ContentStart, EditorContent.ContentEnd);
            _fileHelper.SaveFile(textRange.Text);
        }

        public void CloseCommandAction()
        {
            EditorContent = new FlowDocument();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
