﻿using CNC.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CNC.ViewModel
{
    public class CommunicationViewModel : INotifyPropertyChanged
    {
        public CommunicationViewModel(CommunicationCore communicationCore)
        {
            _communicationCore = communicationCore;
        }

        ~CommunicationViewModel()
        {
            cts.Cancel();
        }

        private CancellationTokenSource cts = new CancellationTokenSource();
        private CommunicationCore _communicationCore;

        private ICommand _connectComCommand;
        public ICommand ConnectComCommand
        {
            get
            {
                return _connectComCommand ?? (_connectComCommand = new CommandHandler(() => ConnectComActionAsync(), true));
            }
        }

        public void ConnectComActionAsync()
        {
            string portName = ComPortNameSelected;
            int baudrate = ComPortBaudrateSelected;

            if (_communicationCore.IsConnected)
            {
                Task.Run(async () => {
                    IsConnected = await _communicationCore.CloseComPortAsync();
                });
            }
            else
            {
                Task.Run(async () => {
                    IsConnected = await _communicationCore.OpenComPortAsync(portName, baudrate);
                });
            }
        }

        private ICommand _connectEthCommand;
        public ICommand ConnectEthCommand {
            get
            {
                return _connectEthCommand ?? (_connectEthCommand = new CommandHandler(() => ConnectEthAction(), true));
            }
        }

        public void ConnectEthAction()
        {
            // Call connect in model; result pass to is connected
            IsConnected = !IsConnected; // for testing
        }

        public ObservableCollection<string> ComPortNames
        {
            get;
            set;
        }

        public ObservableCollection<int> ComPortBaudrates
        {
            get
            {
                var baudrates = new List<int>
                {
                    9600,
                    19200,
                    57600,
                    115200
                };
                return new ObservableCollection<int>(baudrates);
            }
        }

        private int _comPortNameSelectedIndex;
        public int ComPortNameSelectedIndex
        {
            get
            {
                return _comPortNameSelectedIndex;
            }
            set
            {
                _comPortNameSelectedIndex = value;
                OnPropertyChanged(nameof(IsConnected));
            }
        }

        public string ComPortNameSelected
        {
            get;
            set;
        }

        public int ComPortBaudrateSelectedIndex
        {
            get;
            set;
        }

        public int ComPortBaudrateSelected
        {
            get;
            set;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private bool _isConnected = false;
        public bool IsConnected
        {
            get
            {
                return _isConnected;
            }

            set
            {
                _isConnected = value;
                OnPropertyChanged(nameof(IsConnected));
            }
        }

        public void LoadComPortNames()
        {
            var portNames = _communicationCore.GetAvailableComPortNames();
            ComPortNames = new ObservableCollection<string>(portNames);
        }

        public void StartComsUpdateTask()
        {
            Task<Task> task = Task.Factory.StartNew(async () =>
            {
                while (!cts.IsCancellationRequested)
                {
                    await Task.Delay(2000);

                    if (!IsConnected)
                    {
                        continue;
                    }

                    LoadComPortNames();
                }
            }, TaskCreationOptions.LongRunning);

            Task actualTask = task.Unwrap();
        }
    }

    public class CommandHandler : ICommand
    {
        private readonly Action _action;
        private readonly bool _canExecute;
        public CommandHandler(Action action, bool canExecute)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            _action();
        }
    }
}
