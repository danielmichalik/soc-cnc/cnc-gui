﻿using CNC.Communication;
using CNC.Model;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CNC.ViewModel
{
    public class RunControlViewModel : INotifyPropertyChanged
    {
        public RunControlViewModel(CommunicationCore communicationCore)
        {
            _communicationCore = communicationCore;
        }

        private CommunicationCore _communicationCore;

        private ICommand _startCommand;
        public ICommand StartCommand
        {
            get
            {
                return _startCommand ?? (_startCommand = new CommandHandler(() => SendCommand(CommandType.START), true));
            }
        }

        private ICommand _stopCommand;
        public ICommand StopCommand
        {
            get
            {
                return _stopCommand ?? (_stopCommand = new CommandHandler(() => SendCommand(CommandType.STOP), true));
            }
        }

        private ICommand _stepCommand;
        public ICommand StepCommand
        {
            get
            {
                return _stepCommand ?? (_stepCommand = new CommandHandler(() => SendCommand(CommandType.STEP), true));
            }
        }

        private ICommand _resetCommand;
        public ICommand ResetCommand
        {
            get
            {
                return _resetCommand ?? (_resetCommand = new CommandHandler(() => SendCommand(CommandType.RESET), true));
            }
        }

        private void SendCommand(CommandType commandType)
        {
            if (!_communicationCore.IsConnected)
            {
                MessageBox.Show("Systém není připojen!", "Chyba");
                return;
            }

            Task.Run(async () => {
                while (_communicationCore.IsBlocked) ;
                var response = await _communicationCore.TransactionComPort(new Command(commandType));
                if (response.Type == CommandType.ERR)
                {
                    MessageBox.Show($"Při zpracování příkazu {Enum.GetName(typeof(CommandType), commandType)} došlo k chybě!", "Chyba");
                }
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
