﻿using CNC.Communication;
using CNC.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CNC.ViewModel
{
    public class SpeedViewModel : INotifyPropertyChanged
    {
        public SpeedViewModel(CommunicationCore communicationCore)
        {
            _communicationCore = communicationCore;
        }

        private CommunicationCore _communicationCore;

        private int _speed = 100;
        public int Speed
        {
            get
            {
                return _speed;
            }
            set
            {
                try
                {
                    if ((value < 0) || (value > 100))
                    {
                        throw new ArgumentOutOfRangeException("Speed");
                    }
                    _speed = value;
                    OnPropertyChanged(nameof(Speed));
                }
                catch(Exception ex)
                {
                    MessageBox.Show($"Chybně zadaná rychlost! Vyjímka: {ex}", "Chyba");
                }
            }
        }

        private ICommand _setSpeed;
        public ICommand SetSpeed
        {
            get
            {
                return _setSpeed ?? (_setSpeed = new CommandHandler(() => SetSpeedCommandAction(), true));
            }
        }

        private void SetSpeedCommandAction()
        {
            if (!_communicationCore.IsConnected)
            {
                MessageBox.Show("Systém není připojen!", "Chyba");
                return;
            }

            Task.Run(async () =>
            {
                var listOfArgs = new List<Argument>() { new Argument(_speed / 100.0) };
                var command = new Command(CommandType.SET_SPD_MULT, listOfArgs);
                while (_communicationCore.IsBlocked) ;
                var response = await _communicationCore.TransactionComPort(command);
                if (response.Type == CommandType.ERR)
                {
                    MessageBox.Show($"Při zpracování příkazu SET_SPD_MULT došlo k chybě!", "Chyba");
                }
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
