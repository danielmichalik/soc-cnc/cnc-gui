﻿using CNC.Model;
using CNC.ViewModel;
using System.Windows;

namespace CNC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CommunicationCore _communicationCore;
        private FileHelper _fileHelper;
        private GCodeKeep _gCodeKeep;

        public MainWindow()
        {
            InitializeComponent();
            _communicationCore = new CommunicationCore();
            _fileHelper = new FileHelper();
        }

        private void CommunicationViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            _gCodeKeep = new GCodeKeep(_communicationCore, _fileHelper);
            var communicationViewModelObject = new CommunicationViewModel(_communicationCore);
            var editorViewModelObject = new EditorViewModel(_fileHelper, _communicationCore, _gCodeKeep);
            var coordViewModelObject = new CoordViewModel(_communicationCore);
            var runControlViewModelObject = new RunControlViewModel(_communicationCore);
            var speedViewModelObject = new SpeedViewModel(_communicationCore);
            var toolControlViewModelObject = new ToolControlViewModel(_communicationCore);

            CommunicationViewControl.DataContext = communicationViewModelObject;
            EditorViewControl.DataContext = editorViewModelObject;
            CoordViewControl.DataContext = coordViewModelObject;
            RunControlViewControl.DataContext = runControlViewModelObject;
            SpeedViewControl.DataContext = speedViewModelObject;
            ToolControlViewControl.DataContext = toolControlViewModelObject;

            communicationViewModelObject.LoadComPortNames();
            communicationViewModelObject.StartComsUpdateTask();
            coordViewModelObject.StartCoordUpdateTask();
            _gCodeKeep.StartCyclicSending();
        }
    }
}
