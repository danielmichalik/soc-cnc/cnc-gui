﻿using CNC.Communication;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CNC.Model
{
    public class CommunicationCore
    {
        public CommunicationCore()
        {
            _serialPort.DataReceived += _serialPort_DataReceived;
        }

        private SerialPort _serialPort = new SerialPort();
        private string _rxBuffer = string.Empty;

        public bool IsBlocked { get; set; } = false;

        public bool IsConnected
        {
            get
            {
                return _serialPort.IsOpen;
            }
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            _serialPort.WriteTimeout = 500;
            _serialPort.ReadTimeout = 500;
            _rxBuffer += _serialPort.ReadExisting();
        }

        public List<string> GetAvailableComPortNames()
        {
            var portNames = SerialPort.GetPortNames();
            var portNamesList = new List<string>(portNames)
            {
                "COM19"
            };
            return portNamesList;
        }

        public async Task<bool> OpenComPortAsync(string name, int baudrate) // other default => 8N1
        {
            _serialPort.PortName = name;
            _serialPort.BaudRate = baudrate;
            try
            {
                await Task.Run(() =>_serialPort.Open());
            }
            catch(Exception ex)
            {
                MessageBox.Show($"Nastala vyjímka: {ex.ToString()}", "Otevírání COM portu: Chyba");
            }
            return _serialPort.IsOpen;
        }

        public async Task<Command> TransactionComPort(Command request)
        {
            IsBlocked = true;
            FlushRxBuffer();

            _serialPort.Write(request.ToString());

            await Task.Run(() =>
            {
                var cts = new CancellationTokenSource(3000).Token;
                while (true)
                {
                    if (_rxBuffer.Contains("\u0003"))
                    {
                        break;
                    }
                    cts.ThrowIfCancellationRequested();
                }
            });

            var response = Command.Parse(_rxBuffer);
            FlushRxBuffer();
            IsBlocked = false;
            return response;
        }

        private void FlushRxBuffer()
        {
            _rxBuffer = string.Empty;
        }

        public async Task<bool> CloseComPortAsync()
        {
            try
            {
                await Task.Run(() => _serialPort.Close());
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Nastala vyjímka: {ex.ToString()}", "Zavírání COM portu: Chyba");
            }
            return _serialPort.IsOpen;
        }
    }
}
