﻿using CNC.Communication;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CNC.Model
{
    public class GCodeKeep
    {
        public GCodeKeep(CommunicationCore communicationCore, FileHelper fileHelper)
        {
            _communicationCore = communicationCore;
            _fileHelper = fileHelper;
        }

        ~GCodeKeep()
        {
            cts.Cancel();
        }

        public List<Command> _commandBuffer { get; private set; }
        private int index = 0;
        private CancellationTokenSource cts = new CancellationTokenSource();
        private CommunicationCore _communicationCore;
        private FileHelper _fileHelper;

        public void LoadGCode()
        {
            _commandBuffer = new List<Command>();
            using (var streamReader = new StreamReader(_fileHelper.Path))
            {
                string line = streamReader.ReadLine();
                while (line != null)
                {
                    var regex = new Regex("[GMXYZF][\\d\\.]+");
                    var matches = regex.Matches(line);
                    if (matches.Count > 0)
                    {
                        var listOfArguments = new List<Argument>();
                        foreach(var match in matches)
                        {
                            var matchValue = match.ToString();
                            var argument = Argument.Parse(matchValue);
                            if ((listOfArguments.Count > 0) && (matchValue.Contains("G")))
                            {
                                _commandBuffer.Add(new Command(CommandType.G_CODE, listOfArguments));
                                listOfArguments = new List<Argument>();
                            }
                            listOfArguments.Add(argument);
                        }
                        if (listOfArguments.Count > 0)
                        {
                            _commandBuffer.Add(new Command(CommandType.G_CODE, listOfArguments));
                        }
                    }
                    line = streamReader.ReadLine();
                }
                index = 0;
            }
        }

        public void StartCyclicSending()
        {
            Task<Task> task = Task.Factory.StartNew(async () =>
            {
                while (!cts.IsCancellationRequested)
                {
                    await Task.Delay(100);

                    if (!_communicationCore.IsConnected)
                    {
                        continue;
                    }

                    var response = await _communicationCore.TransactionComPort(new Command(CommandType.GET_BUFF));
                    if (response.Type == CommandType.GET_BUFF)
                    {
                        var capacity = response.Arguments[0].IntNumber;
                        var occupied = response.Arguments[1].IntNumber;

                        if (occupied < capacity)
                        {
                            await SendNextGcodeAsync();
                        }
                    }
                }
            }, TaskCreationOptions.LongRunning);

            Task actualTask = task.Unwrap();
        }

        private async Task SendNextGcodeAsync()
        {
            var response = await _communicationCore.TransactionComPort(_commandBuffer[index]);

            if (response.Type != CommandType.G_CODE)
            {
                await _communicationCore.TransactionComPort(new Command(CommandType.STOP));
                MessageBox.Show($"Při odesílání příkazu {index} došlo k chybě! Program je zastaven!", "Chyba");
                return;
            }

            if (index >= _commandBuffer.Count)
            {
                MessageBox.Show("Odesílání programu je hotové!", "Hotovo");
            }
        }
    }
}
