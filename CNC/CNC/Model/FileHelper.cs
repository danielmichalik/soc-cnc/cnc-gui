﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;

namespace CNC.Model
{
    public class FileHelper
    {
        public FileHelper()
        { }

        public FileHelper(string path)
        {
            Path = path;
        }

        public string Path { get; private set; } = string.Empty;

        public string OpenAndReadFile()
        {
            ShowOpenFileDialog();
            if (Path != string.Empty)
            {
                var content = string.Empty;
                using (var streamReader = new StreamReader(Path))
                {
                    content = streamReader.ReadToEnd();
                }
                return content;
            }
            return string.Empty;
        }

        public void SaveFile(string content)
        {
            if (Path == string.Empty)
            {
                ShowOpenFileDialog();
            }

            if (Path == string.Empty)
            {
                MessageBox.Show("Soubor nebyl uložen!", "Chyba");
                return;
            }

            using (var streamWriter = new StreamWriter(Path))
            {
                streamWriter.Write(content);
            }
        }

        public void ShowOpenFileDialog()
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "G-code files (*.gcode)|*.gcode|All files (*.*)|*.*",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            };
            if (openFileDialog.ShowDialog() == true)
            {
                Path = openFileDialog.FileName;
            }
            else
            {
                Path = string.Empty;
            }
        }

        private void ShowSaveFileDialog()
        {
            var saveFileDialog = new SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            };
            if (saveFileDialog.ShowDialog() == true)
            {
                Path = saveFileDialog.FileName;
            }
        }
    }
}
