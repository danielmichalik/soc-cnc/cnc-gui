﻿using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace CNC.View
{
    /// <summary>
    /// Interaction logic for EditorView.xaml
    /// </summary>
    public partial class EditorView : UserControl
    {
        public static FlowDocument GetDocumentContent(DependencyObject dependencyObject)
        {
            return (FlowDocument)dependencyObject.GetValue(DocumentContentProperty);
        }

        public static void SetDocumentContent(DependencyObject dependencyObject, FlowDocument value)
        {
            dependencyObject.SetValue(DocumentContentProperty, value);
        }

        public static readonly DependencyProperty DocumentContentProperty =
          DependencyProperty.RegisterAttached(
            "DocumentContent",
            typeof(FlowDocument),
            typeof(EditorView),
            new FrameworkPropertyMetadata
            {
                BindsTwoWayByDefault = true,
                PropertyChangedCallback = (obj, e) =>
                {
                    // from VM to view
                    var richTextBox = (RichTextBox)obj;
                    var documentContent = GetDocumentContent(richTextBox);
                    richTextBox.Document = documentContent;
                    richTextBox.TextChanged += (obj2, e2) =>
                          {
                              if (richTextBox.Document == documentContent)
                              {
//                                  MemoryStream memoryStream = new MemoryStream();
//                                  textRange.Save(memoryStream, DataFormats.Text);
                                  SetDocumentContent(richTextBox, documentContent);
                              }
                          };
                }
            });

        public EditorView()
        {
            InitializeComponent();
        }
    }
}
