﻿using CNC.Model;
using CNC.Communication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace CNC.Test
{
    [TestClass]
    public class CommunicationCoreTests
    {
        [TestMethod]
        public async Task CommandTransactioTestAsync()
        {
            var communicationCore = new CommunicationCore();
            var response = await communicationCore.TransactionComPort(new Command(CommandType.ECHO));
            Assert.IsTrue(response.Type == CommandType.ECHO);
        }
    }
}
