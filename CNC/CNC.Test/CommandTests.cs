﻿using CNC.Communication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace CNC.Test
{
    [TestClass]
    public class CommandTests
    {
        [TestMethod]
        public void EchoCommandToStringWorks()
        {
            var command = new Command(CommandType.ECHO);
            Assert.AreEqual("\u0002ECHO\u0003\r\n", command.ToString());
        }

        [TestMethod]
        public void GCodeCommandToStringWorks()
        {
            var argumentsList = new List<Argument>
            {
                new Argument("X", 25.5),
                new Argument("Y", 25)
            };
            var command = new Command(CommandType.G_CODE, argumentsList);
            Assert.AreEqual("\u0002G_CODE;X25,500;Y25\u0003\r\n", command.ToString());
        }

        [TestMethod]
        public void ParseCommandWorksForEcho()
        {
            var testCommandString = "\u0002ECHO\u0003";
            var parseResult = Command.Parse(testCommandString);
            Assert.IsNotNull(parseResult);
        }

        [TestMethod]
        public void ParseCommandWorksForGcode()
        {
            var testCommandString = "\u0002G_CODE;X25,500;Y25\u0003\r\n";
            var parseResult = Command.Parse(testCommandString);
            Assert.IsNotNull(parseResult);
            Assert.AreEqual(CommandType.G_CODE, parseResult.Type);
            Assert.AreEqual("X", parseResult.Arguments[0].StringPart);
            Assert.AreEqual(25.5, parseResult.Arguments[0].DoubleNumber, 0.0001);
            Assert.AreEqual("Y", parseResult.Arguments[1].StringPart);
            Assert.AreEqual(25, parseResult.Arguments[1].IntNumber);
        }

        [TestMethod]
        public void ParseExtendedCommandWorksForGcode()
        {
            var testCommandString = "fsdfsdfs\u0002G_CODE;X25,500;Y25\u0003\r\ndsfdsfsdfsds";
            var parseResult = Command.Parse(testCommandString);
            Assert.IsNotNull(parseResult);
            Assert.AreEqual(CommandType.G_CODE, parseResult.Type);
            Assert.AreEqual("X", parseResult.Arguments[0].StringPart);
            Assert.AreEqual(25.5, parseResult.Arguments[0].DoubleNumber, 0.0001);
            Assert.AreEqual("Y", parseResult.Arguments[1].StringPart);
            Assert.AreEqual(25, parseResult.Arguments[1].IntNumber);
        }
    }
}
