﻿using CNC.Communication;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CNC.Test
{
    [TestClass]
    public class ArgumentTests
    {
        [TestMethod]
        public void ToStringWorksForIntNumberArgument()
        {
            var argument = new Argument(5);
            Assert.AreEqual("5", argument.ToString());
        }

        [TestMethod]
        public void ToStringWorksForDoubleNumberArgument()
        {
            var argument = new Argument(5.123456789);
            Assert.AreEqual("5,123", argument.ToString());
        }

        [TestMethod]
        public void ToStringWorksForNamedIntNumberArgument()
        {
            var argument = new Argument("PR", 5);
            Assert.AreEqual("PR5", argument.ToString());
        }

        [TestMethod]
        public void ToStringWorksForNamedDoubleNumberArgument()
        {
            var argument = new Argument("PR", 5.123456789);
            Assert.AreEqual("PR5,123", argument.ToString());
        }

        [TestMethod]
        public void ToStringWorksForStringOnlyArgument()
        {
            var argument = new Argument("ABCD");
            Assert.AreEqual("ABCD", argument.ToString());
        }

        [TestMethod]
        public void ParseWorksForIntNumberArgument()
        {
            var argument = Argument.Parse("5");
            Assert.AreEqual(5, argument.IntNumber);
        }

        [TestMethod]
        public void ParseWorksForDotDoubleNumberArgument()
        {
            var argument = Argument.Parse("5.123");
            Assert.AreEqual(5.123, argument.DoubleNumber);
        }

        [TestMethod]
        public void ParseWorksForCommaDoubleNumberArgument()
        {
            var argument = Argument.Parse("5,123456789");
            Assert.AreEqual(5.123456789, argument.DoubleNumber);
        }

        [TestMethod]
        public void ParseWorksForNamedIntNumberArgument()
        {
            var argument = Argument.Parse("PR5");
            Assert.AreEqual("PR", argument.StringPart);
            Assert.AreEqual(5, argument.IntNumber);
        }

        [TestMethod]
        public void ParseWorksForNamedDotDoubleNumberArgument()
        {
            var argument = Argument.Parse("PR5.123");
            Assert.AreEqual("PR", argument.StringPart);
            Assert.AreEqual(5.123, argument.DoubleNumber);
        }

        [TestMethod]
        public void ParseWorksForNamedCommaDoubleNumberArgument()
        {
            var argument = Argument.Parse("PR5,123");
            Assert.AreEqual("PR", argument.StringPart);
            Assert.AreEqual(5.123, argument.DoubleNumber);
        }

        [TestMethod]
        public void ParseWorksForStringOnlyArgument()
        {
            var argument = new Argument("ABCD");
            Assert.AreEqual("ABCD", argument.ToString());
        }
    }
}
