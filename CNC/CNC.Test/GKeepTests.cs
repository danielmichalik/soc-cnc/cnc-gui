﻿using CNC.Model;
using CNC.Communication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System;

namespace CNC.Test
{
    [TestClass]
    public class GKeepTests
    {
        [TestMethod]
        public void ParseTestFile()
        {
            var communicationCore = new CommunicationCore();
            var tt = Environment.CurrentDirectory;
            var fileHelper = new FileHelper(@"testTemplate.gcode");
            var gCodeKeep = new GCodeKeep(communicationCore, fileHelper);
            gCodeKeep.LoadGCode();
            Assert.AreEqual(5, gCodeKeep._commandBuffer.Count);
            var listOfCommands = gCodeKeep._commandBuffer;
            Assert.IsTrue(listOfCommands.All(c => c.Type == CommandType.G_CODE));
            Assert.AreEqual("\u0002G_CODE;G17\u0003\r\n", gCodeKeep._commandBuffer[0].ToString());
            Assert.AreEqual("\u0002G_CODE;G21\u0003\r\n", gCodeKeep._commandBuffer[1].ToString());
            Assert.AreEqual("\u0002G_CODE;G90\u0003\r\n", gCodeKeep._commandBuffer[2].ToString());
            Assert.AreEqual("\u0002G_CODE;M3;F200\u0003\r\n", gCodeKeep._commandBuffer[3].ToString());
            Assert.AreEqual("\u0002G_CODE;G0;X10;Y10,560;Z10\u0003\r\n", gCodeKeep._commandBuffer[4].ToString());
        }
    }
}
